function [ x ] = objectiveFunc4( v )

global ice;
global timeData;

a = v(1);   %wyraz wolny
b = v(2);   %miesiac
c = v(3);   %dzien roku
d = v(4);   %sinus(dzienRoku)
f = v(5);   %kat dlugosci geograficznej
g = v(6);   %opoznienie o 365 dni

model = zeros(100, 361);        %model na razie tylko dla ostatnich 100 obs.

for t = 1:100
   for k = 1:361
      model(t,k) = a...
          + b * timeData(8530+t,2)...
          + c * timeData(8530+t,4)...
          + d * sind(timeData(8530+t,4))...
          + f * k...
          + g * ice(8530+t-365,k);
   end
end

x = 0;
for t=1:100
    for k = 1:361
        x = x + abs(ice(8530+t,k) - model(t,k));
    end
end

end