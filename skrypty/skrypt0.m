
for t=1:9530
   for dlugosc=1:361
       if ice(t, dlugosc) < min(1, dlugosc)
           min(1,dlugosc) = ice(t,dlugosc);
       end   
   end
end

xx = zeros(1,361);
yy = zeros(1,361);
for k = 1:361
    xx(k) = min(k) * cosd(k);
    yy(k) = min(k) * sind(k);
end

figure
hold on
plot (-xx, yy, '-');
axis([-3000 3000 -3000 3000]);
