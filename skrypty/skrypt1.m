global ice;                 %macierz tylko z szerokosciami lodu 
ice = danez1(:,5:365);
degrees = 1:361;            %d�ugosci geograficzne

global timeData;
timeData = danez1(:,1:4);     %rok, miesiac, dzien, dzien roku

global idx;                   %dana dlugosc dla ktorej liczymy lod
idx = 1;

obj = @objectiveFunc1;              %funkcja celu. im mniejsza, tym bli�ej celu
nvar = 4;                           %ilo�� szukanych parametr�w
lb = [-90 -5 -5 -5];                %ograniczenie warto�ci szukanych param. z do�u
ub = [90 5 5 5];                    %ograniczenie warto�ci szukanych param. z g�ry
options = optimoptions('particleswarm', 'SwarmSize',100, 'Display', 'iter'); %100 cz�steczek

model = zeros(7794,361);            %macierz na modele wszystkich katow
coefficients = zeros(361,4);        %macierz na wsp�czynniki a,b,c,d

for idx=1:361
    [x,fval] = particleswarm(obj, nvar, lb, ub, options);
    coefficients(idx, :) = x;
    model(:,idx) = x(1) + x(2) .* timeData(:,2) + x(3)...
        .* timeData(:,4) + x(4) * sind(timeData(:,4)); 
    idx
end

%% 
axisX = 1:7794;
plot(axisX, model(:,1), 'red', axisX, ice(:,1), 'green');
legend('predykcje', 'obserwowane');
axis([0 8000 -71 -50]);

%%

dlmwrite('modele_z_zadania_1.txt', model);
dlmwrite('wspolczynniki_z_zadania_1.txt', coefficients);

xlswrite('modele_zadanie1.xlsx', model);
xlswrite('wspolczynniki_zadanie1.xlsx', coefficients);

%%

%wszystkie modele dla wszystkich rekordow
modelKM = zeros(9350,361);

%caly lod w kilometrach
iceKM = 90 + danez1(:,5:365);
iceKM = iceKM .* 111.69;
allTimeData = danez1(:,1:4); 

for t=1:9530
    for lon=1:361
        %wyliczenie modelu na podst. wspolczynnikow
        modelKM(t,lon) =  coefficients(lon, 1)...
            + coefficients(lon, 2) .* allTimeData(t,2)...
            + coefficients(lon, 3) .* allTimeData(t,4)...
            + coefficients(lon, 4) * sind(allTimeData(t,4));
        %zamiana na kilometry od bieguna
        modelKM(t,lon) = 90 + modelKM(t,lon);
        modelKM(t,lon) = modelKM(t, lon) .* 111.69;
    
    end
end


%%

contourX = zeros(9350, 361);            %wsp. X dla modeli
contourY = zeros(9350, 361);            %wsp. Y dla modeli
contourXreal = zeros(9350, 361);        %wsp. X dla obserwacji
contourYreal = zeros(9350, 361);        %wsp. Y dla obserwacji

for t = 1:9350
    for lon = 1:361
        contourX(t,lon) = modelKM(t,lon) * cosd(lon);
        contourY(t,lon) = modelKM(t,lon) * sind(lon);
        
        contourXreal(t,lon) = iceKM(t,lon) * cosd(lon);
        contourYreal(t,lon) = iceKM(t,lon) * sind(lon);
    end
end




%%

for i=1:9350
    plot (-contourXreal(i,:), contourYreal(i,:), 'blue', -contourX(i,:), contourY(i,:), 'red');
    legend(strcat('obserwacja nr ', num2str(i)), strcat('model nr ', num2str(i)));
    axis([-5000 5000 -5000 5000]);
    pause(0.001);
end


%%

i=280;
plot (-contourXreal(i,:), contourYreal(i,:), 'blue', -contourX(i,:), contourY(i,:), 'red');
legend(strcat('obserwacja nr ', num2str(i)), strcat('model nr ', num2str(i)));
axis([-5000 5000 -5000 5000]);







