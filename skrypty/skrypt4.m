
global ice;                 %macierz tylko z szerokosciami lodu 
ice = danez1(:,5:365);
degrees = 1:361;            %d�ugosci geograficzne
global timeData;
timeData = danez1(:,1:4);     %rok, miesiac, dzien, dzien roku

%%

obj = @objectiveFunc4;           %funkcja celu. im mniejsza, tym bli�ej celu
nvar = 6;                        %ilo�� szukanych parametr�w
lb = [-100 -10 -10 -10];         %ograniczenie warto�ci szukanych param. z do�u
ub = [100 10 10 10];             %ograniczenie warto�ci szukanych param. z g�ry
options = optimoptions('particleswarm', 'SwarmSize', 50, 'Display', 'iter'); %100 cz�steczek


[x,fval] = particleswarm(obj, nvar, lb, ub, options);

model = zeros(9165, 361);

for t = 366:9530
   for k = 1:361
      model(t-365,k) = x(1)...
          + x(2) * timeData(t,2)...
          + x(3) * timeData(t,4)...
          + x(4) * sind(timeData(t,4))...
          + x(5) * k...
          + x(6) * ice(t-365,k);
   end
end


%% 2

axisX = 1:9165;
plot(axisX, model(:,5), 'red', axisX, ice(366:9530,5), 'green');
legend('predykcje', 'obserwowane');


%% 3

%wszystkie modele dla wszystkich rekordow
modelKM = model;
modelKM = 90 + modelKM;
modelKM = modelKM .* 111.69;

%caly lod w kilometrach
iceKM = 90 + danez1(366:9530,5:365);
iceKM = iceKM .* 111.69;
allTimeData = danez1(366:9530,1:4); 


%% 4

contourX = zeros(9165, 361);            %wsp. X dla modeli
contourY = zeros(9165, 361);            %wsp. Y dla modeli
contourXreal = zeros(9165, 361);        %wsp. X dla obserwacji
contourYreal = zeros(9165, 361);        %wsp. Y dla obserwacji

for t = 1:9165
    for lon = 1:361
        contourX(t,lon) = modelKM(t,lon) * cosd(lon);
        contourY(t,lon) = modelKM(t,lon) * sind(lon);
        
        contourXreal(t,lon) = iceKM(t,lon) * cosd(lon);
        contourYreal(t,lon) = iceKM(t,lon) * sind(lon);
    end
end

%% 5

for i=1:9165
    plot (-contourXreal(i,:), contourYreal(i,:), 'blue', -contourX(i,:), contourY(i,:), 'red');
    legend(strcat('obserwacja nr ', num2str(i)), strcat('model nr ', num2str(i)));
    axis([-5000 5000 -5000 5000]);
    pause(0.001);
end 


%% 5.5

i=280;
plot (-contourXreal(i,:), contourYreal(i,:), 'blue', -contourX(i,:), contourY(i,:), 'red');
legend(strcat('obserwacja nr ', num2str(i)), strcat('model nr ', num2str(i)));
axis([-5000 5000 -5000 5000]);


%% 6

dlmwrite('modele_z_zadania_4.txt', model);
dlmwrite('wspolczynniki_z_zadania_4.txt', x);
xlswrite('modele_zadanie4.xlsx', model);
xlswrite('wspolczynniki_zadanie4.xlsx', x);

