function [ x ] = objectiveFunc1( v )

global ice;
global idx;
global timeData;

a = v(1);
b = v(2);
c = v(3);
d = v(4);

model = a + b .* timeData(:,2) + c.* timeData(:,4) + d * sind(timeData(:,4));
x = 0;

for j=1:7794
    x = x + abs(ice(j,idx) - model(j));
end

end

